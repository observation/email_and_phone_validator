package sample.validator.app;

import java.util.LinkedList;
import java.util.List;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;

class EmailValidator {
    static String isValidEmailAddress(String email) {

        try {
            InternetAddress emailAddress = new InternetAddress(email);
            emailAddress.validate();
        } catch (AddressException exception) {
            return "[" + email + "] => INVALID::" + exception.getMessage();
        }

        return "[" + email + "] => " + "VALID";
    }
}

class Phone {
    private final String region;
    private final String phone;

    public Phone(String region, String phone) {
        this.region = region;
        this.phone = phone;
    }

    public String getRegion() {
        return this.region;
    }

    public String getPhone() {
        return this.phone;
    }

    @Override
    public String toString() {
        return "Phone [region=" + region + ", phone=" + phone + "]";
    }
}

class PhoneValidator {
    static String validate(String regionCode, String phoneNumberString) {

        if (regionCode == null ||
                regionCode.isEmpty() ||
                phoneNumberString == null ||
                phoneNumberString.isEmpty()) {
            return "ERROR -> Either \"Region Code\" or \"Phone Number\" is either \"empty\" or \"null\".";
        }

        if (!(regionCode.length() == 2 && regionCode.matches("[a-zA-Z]+"))) {
            return "ERROR -> \"Region code\" should exactly conatin 2 alphabate characters.";
        }

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        regionCode = regionCode.toUpperCase();
        if (!phoneNumberUtil.getSupportedRegions().contains(regionCode)) {
            return "Error -> Invalid \"Region Code\".";
        }

        PhoneNumber phoneNumber = null;
        try {
            phoneNumber = phoneNumberUtil.parse(phoneNumberString, regionCode);
        } catch (NumberParseException exception) {
            return "Error -> Invalid \"Phone Number\".";
        }

        if (!phoneNumberUtil.isPossibleNumberForType(phoneNumber, PhoneNumberType.MOBILE)) {
            return "Error -> Provided \"Phone Number\" is not a mobile number.";
        }

        if (phoneNumberUtil.getCountryCodeForRegion(regionCode) != phoneNumber.getCountryCode()) {
            return "Error -> Provided \"Phone Number\" has invalid \"Region code\".";
        }

        return "{ COUNTRY_CODE = \"" + phoneNumber.getCountryCode()
                + "\", PHONE_NUMBER = \"" + phoneNumber.getNationalNumber()
                + "\" }";
    }
}

public class Main {
    public static void main(String[] args) {

        System.out.println("\nValid and Invalid Emails:\n");

        List<String> emailAddresses = new LinkedList<>();

        // Invalid Email IDs.
        emailAddresses.add("username");
        emailAddresses.add("email.com");
        emailAddresses.add("@email.com");
        emailAddresses.add(".user@email.com");
        emailAddresses.add("username@email.com.");
        emailAddresses.add("username@email.com_");
        emailAddresses.add("user@name@email.com");
        emailAddresses.add("user(name@email.com");
        emailAddresses.add("user)name@email.com");
        emailAddresses.add("user()name@email.com");
        emailAddresses.add("user[name@email.com");
        emailAddresses.add("user]name@email.com");
        emailAddresses.add("user[]name@email.com");
        emailAddresses.add("user,name@email.com");
        emailAddresses.add("user:name@email.com");
        emailAddresses.add("user;name@email.com");

        // Valid Email IDs.
        emailAddresses.add("user@email.com");
        emailAddresses.add("user328@email.com");
        emailAddresses.add("username@email.com");
        emailAddresses.add("username328@email.com");
        emailAddresses.add("user.name328@email.com");
        emailAddresses.add("user_name328@email.com");
        emailAddresses.add("UserName@email.com");
        emailAddresses.add("UserName328@email.com");
        emailAddresses.add("User.Name328@email.com");
        emailAddresses.add("User_Name328@email.com");
        emailAddresses.add("user.name@email.com");
        emailAddresses.add("user_name@email.com");
        emailAddresses.add("01user@email.com");
        emailAddresses.add("_user@email.com");
        emailAddresses.add("username@email.com01");
        emailAddresses.add("username@email.co.biz");
        emailAddresses.add("user?name@email.com");
        emailAddresses.add("user#name@email.com");
        emailAddresses.add("user*name@email.com");
        emailAddresses.add("user$name@email.com");
        emailAddresses.add("user^name@email.com");
        emailAddresses.add("user|name@email.com");
        emailAddresses.add("user/name@email.com");
        emailAddresses.add("user%name@email.com");
        emailAddresses.add("user~name@email.com");
        emailAddresses.add("user`name@email.com");
        emailAddresses.add("user!name@email.com");
        emailAddresses.add("user+name@email.com");
        emailAddresses.add("user-name@email.com");
        emailAddresses.add("user=name@email.com");
        emailAddresses.add("user{name@email.com");
        emailAddresses.add("user}name@email.com");
        emailAddresses.add("user{}name@email.com");

        for (String emailAddress : emailAddresses) {
            System.out.println(EmailValidator.isValidEmailAddress(emailAddress));
        }

        System.out.println("\n\nValid and Invalid Phone Numbers:\n");

        List<Phone> phones = new LinkedList<>();

        // Empty/Null region code and/or phone numbers.
        phones.add(new Phone(null, "1020304050"));
        phones.add(new Phone("IN", null));
        phones.add(new Phone(null, null));
        phones.add(new Phone("", "1020304050"));

        // Invalid region code format.
        phones.add(new Phone("IN", ""));
        phones.add(new Phone("in", ""));
        phones.add(new Phone("", ""));
        phones.add(new Phone("I", "1020304050"));
        phones.add(new Phone("i", "1020304050"));
        phones.add(new Phone("IND", "1020304050"));
        phones.add(new Phone("ind", "1020304050"));
        phones.add(new Phone("9", "1020304050"));
        phones.add(new Phone("1", "1020304050"));
        phones.add(new Phone("+91", "1020304050"));
        phones.add(new Phone("91", "1020304050"));
        phones.add(new Phone("A1", "1020304050"));
        phones.add(new Phone("9B", "1020304050"));
        phones.add(new Phone("a1", "1020304050"));
        phones.add(new Phone("9b", "1020304050"));

        // Invalid region code.
        phones.add(new Phone("ZZ", "0102030405"));

        // Invalid phone numbers.
        phones.add(new Phone("IN", "10203a?C2#"));
        phones.add(new Phone("In", "1a2b3c4d5e"));
        phones.add(new Phone("iN", "10203a?C2#"));
        phones.add(new Phone("in", "1a2b3c4d5e"));

        // Non mobile numbers.
        phones.add(new Phone("IN", "0102030405"));
        phones.add(new Phone("IN", "102030405"));
        phones.add(new Phone("In", "102030405"));
        phones.add(new Phone("iN", "102030405"));
        phones.add(new Phone("in", "102030405"));
        phones.add(new Phone("IN", "10203040506"));
        phones.add(new Phone("In", "10203040506"));
        phones.add(new Phone("iN", "10203040506"));
        phones.add(new Phone("in", "10203040506"));

        // Phone number provided with invalid region code.
        phones.add(new Phone("IN", "+441020304050"));

        // Valid mobile numbers.
        phones.add(new Phone("IN", "1020304050"));
        phones.add(new Phone("In", "1020304050"));
        phones.add(new Phone("iN", "1020304050"));
        phones.add(new Phone("in", "1020304050"));
        phones.add(new Phone("IN", "10203 04050"));
        phones.add(new Phone("IN", "10203-04050"));
        phones.add(new Phone("IN", "(10203) (04050)"));
        phones.add(new Phone("IN", "(10203)-(04050)"));
        phones.add(new Phone("IN", "911020304050"));
        phones.add(new Phone("IN", "+911020304050"));
        phones.add(new Phone("IN", "(91)1020304050"));
        phones.add(new Phone("IN", "(+91)1020304050"));
        phones.add(new Phone("IN", "(91)(10203)(04050)"));
        phones.add(new Phone("IN", "(+91)(10203)(04050)"));
        phones.add(new Phone("IN", "91 10203 04050"));
        phones.add(new Phone("IN", "+91 10203 04050"));
        phones.add(new Phone("IN", "91-10203-04050"));
        phones.add(new Phone("IN", "+91-10203-04050"));
        phones.add(new Phone("IN", "(91) 10203 04050"));
        phones.add(new Phone("IN", "(+91) 10203 04050"));
        phones.add(new Phone("IN", "(91) (10203) (04050)"));
        phones.add(new Phone("IN", "+(91) (10203) (04050)"));
        phones.add(new Phone("IN", "(91)-(10203)-(04050)"));
        phones.add(new Phone("IN", "+(91)-(10203)-(04050)"));
        phones.add(new Phone("iN", "1020AbcdeF"));

        for (Phone phone : phones) {
            System.out.println("\n" + phone + "\n|_______>>> "
                    + PhoneValidator.validate(phone.getRegion(), phone.getPhone()));
        }

        System.out.println("\n");
    }
}